module Main where

import           Graphics.UI.Gtk

main :: IO ()
main = do

    initGUI

    builder <- builderNew
    builderAddFromFile builder "simple.glade"

    mainWindow <- builderGetObject builder castToWindow "window_main"
    buttonApply <- builderGetObject builder castToButton "button_apply"
    buttonAbout <- builderGetObject builder castToButton "button_about"

    on buttonApply buttonActivated $ putStrLn "Apply button pressed!"
    on buttonAbout buttonActivated $ putStrLn "About button pressed!"
    on mainWindow objectDestroy mainQuit

    widgetShowAll mainWindow

    mainGUI
