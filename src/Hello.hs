module Main where

import           Graphics.UI.Gtk

main :: IO ()
main = do

    initGUI

    window <- windowNew
    set window [ windowTitle := "Hello!" ]

    widgetShowAll window

    on window objectDestroy mainQuit

    mainGUI
